/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用Apache-2.0许可证]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_lucener.htm
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zhiqim.lucener;

import java.util.List;

/**
 * Lucene工具类
 *
 * @version v1.0.0 @author zouzhigang 2018-4-14 新建与整理
 */
public class Lucenes
{
    public static String getIdType(List<LuceneField> fieldList)
    {
        for (LuceneField field : fieldList)
        {
            if ("id".equals(field.getName()))
            {
                return field.getType();
            }
        }
        
        return "string";
    }
}
