/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用Apache-2.0许可证]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_lucener.htm
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zhiqim.lucener;

import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DocValuesType;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.util.NumericUtils;

/**
 * Lucene字段类型排序四个常量
 *
 * @version v1.0.0 @author zouzhigang 2018-4-17 新建与整理
 */
public class LuceneFieldType
{
    public static final FieldType TYPE_STORED_SORTED_INT    = new FieldType();
    public static final FieldType TYPE_STORED_SORTED_LONG   = new FieldType();
    public static final FieldType TYPE_STORED_SORTED_FLOAT  = new FieldType();
    public static final FieldType TYPE_STORED_SORTED_DOUBLE = new FieldType();
    
    static 
    {
        TYPE_STORED_SORTED_INT.setTokenized(true);
        TYPE_STORED_SORTED_INT.setOmitNorms(true);
        TYPE_STORED_SORTED_INT.setIndexOptions(IndexOptions.DOCS);
        TYPE_STORED_SORTED_INT.setNumericType(FieldType.NumericType.INT);
        TYPE_STORED_SORTED_INT.setNumericPrecisionStep(NumericUtils.PRECISION_STEP_DEFAULT_32);
        TYPE_STORED_SORTED_INT.setStored(true);
        TYPE_STORED_SORTED_INT.setDocValuesType(DocValuesType.NUMERIC);
        TYPE_STORED_SORTED_INT.freeze();
        
        TYPE_STORED_SORTED_LONG.setTokenized(true);
        TYPE_STORED_SORTED_LONG.setOmitNorms(true);
        TYPE_STORED_SORTED_LONG.setIndexOptions(IndexOptions.DOCS);
        TYPE_STORED_SORTED_LONG.setNumericType(FieldType.NumericType.LONG);
        TYPE_STORED_SORTED_LONG.setStored(true);
        TYPE_STORED_SORTED_LONG.setDocValuesType(DocValuesType.NUMERIC);
        TYPE_STORED_SORTED_LONG.freeze();
        
        TYPE_STORED_SORTED_FLOAT.setTokenized(true);
        TYPE_STORED_SORTED_FLOAT.setOmitNorms(true);
        TYPE_STORED_SORTED_FLOAT.setIndexOptions(IndexOptions.DOCS);
        TYPE_STORED_SORTED_FLOAT.setNumericType(FieldType.NumericType.FLOAT);
        TYPE_STORED_SORTED_FLOAT.setNumericPrecisionStep(NumericUtils.PRECISION_STEP_DEFAULT_32);
        TYPE_STORED_SORTED_FLOAT.setStored(true);
        TYPE_STORED_SORTED_FLOAT.setDocValuesType(DocValuesType.NUMERIC);
        TYPE_STORED_SORTED_FLOAT.freeze();
        
        TYPE_STORED_SORTED_DOUBLE.setTokenized(true);
        TYPE_STORED_SORTED_DOUBLE.setOmitNorms(true);
        TYPE_STORED_SORTED_DOUBLE.setIndexOptions(IndexOptions.DOCS);
        TYPE_STORED_SORTED_DOUBLE.setNumericType(FieldType.NumericType.DOUBLE);
        TYPE_STORED_SORTED_DOUBLE.setStored(true);
        TYPE_STORED_SORTED_DOUBLE.setDocValuesType(DocValuesType.NUMERIC);
        TYPE_STORED_SORTED_DOUBLE.freeze();
    }
}
