/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用Apache-2.0许可证]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_lucener.htm
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zhiqim.lucener;

/**
 * Lucene字段对象
 *
 * @version v1.0.0 @author zouzhigang 2018-4-14 新建与整理
 */
public class LuceneField
{
    private String name;
    private String type;
    private boolean required;
    private boolean indexable;
    private float weight;
    private String desc;
    private boolean sort;
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    public boolean isIndexable()
    {
        return indexable;
    }

    public void setIndexable(boolean indexable)
    {
        this.indexable = indexable;
    }
    
    public float getWeight()
    {
        return weight;
    }

    public void setWeight(float weight)
    {
        this.weight = weight;
    }

    public boolean isRequired()
    {
        return required;
    }
    
    public void setRequired(boolean required)
    {
        this.required = required;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public boolean isSort()
    {
        return sort;
    }

    public void setSort(boolean sort)
    {
        this.sort = sort;
    }
}
