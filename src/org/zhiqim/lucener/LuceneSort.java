/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用Apache-2.0许可证]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_lucener.htm
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zhiqim.lucener;

import org.apache.lucene.search.SortField.Type;

/**
 * Lucene排序字段
 *
 * @version v1.0.0 @author zouzhigang 2018-4-16 新建与整理
 */
public class LuceneSort
{
    private String name;
    private String type;
    private boolean reverse;
    private String desc;
    
    public Type getSortType()
    {
        if ("score".equals(type))
            return Type.SCORE;
        else if ("doc".equals(type))
            return Type.DOC;
        else if ("int".equals(type))
            return Type.INT;
        else if ("float".equals(type))
            return Type.FLOAT;
        else if ("long".equals(type))
            return Type.LONG;
        else if ("double".equals(type))
            return Type.DOUBLE;
//        其他类型暂不支持
//        else if ("string".equals(type))
//            return Type.STRING;
//        else if ("custom".equals(type))
//            return Type.CUSTOM;
//        else if ("string_val".equals(type))
//            return Type.STRING_VAL;
//        else if ("bytes".equals(type))
//            return Type.BYTES;
//        else if ("rewriteable".equals(type))
//            return Type.REWRITEABLE;
        else
            return null;
    }
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public boolean isReverse()
    {
        return reverse;
    }
    public void setReverse(boolean reverse)
    {
        this.reverse = reverse;
    }
    public String getDesc()
    {
        return desc;
    }
    public void setDesc(String desc)
    {
        this.desc = desc;
    }
}
