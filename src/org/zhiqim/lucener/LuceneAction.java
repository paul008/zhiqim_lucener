/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦，本文采用Apache-2.0许可证]
 * 
 * https://zhiqim.org/project/zhiqim_products/zhiqim_lucener.htm
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.zhiqim.lucener;

import java.util.List;

import org.apache.lucene.document.Document;
import org.zhiqim.httpd.HttpRequest;
import org.zhiqim.httpd.context.extend.StdSwitchAction;
import org.zhiqim.httpd.validate.ones.IsNotEmpty;
import org.zhiqim.kernel.Global;
import org.zhiqim.manager.ZmrConstants;

/**
 * Lucene索引页
 *
 * @version v1.0.0 @author zouzhigang 2018-4-14 新建与整理
 */
public class LuceneAction extends StdSwitchAction implements ZmrConstants
{

    @Override
    protected void validateId(HttpRequest request)
    {
        request.addValidate(new IsNotEmpty("id", "索引编号不能为空"));
    }

    @Override
    protected void validateForm(HttpRequest request)
    {
        LuceneServer server = Global.getService(LuceneServer.class);
        List<LuceneField> fieldList = server.getFieldList();
        for (LuceneField field : fieldList)
        {
            if ("id".equals(field.getName()) || !field.isRequired())
                continue;
            
            request.addValidate(new IsNotEmpty(field.getName(), "索引字段["+field.getName()+"]不能为空"));
        }
    }
    
    @Override
    protected void list(HttpRequest request) throws Exception
    {
        int page = request.getParameterInt("page", 1);
        int pageSize = request.getAttributeInt(ZMR_PAGE_SIZE, 20);
        String q = request.getParameterNoFileterOnCNT("q");
        String field = request.getParameter("field");
        int reverse = request.getParameterInt("reverse");
        
        LuceneServer server = Global.getService(LuceneServer.class);
        request.setAttribute("result", server.page(page, pageSize, q, field, reverse==1?true:false));
        request.setAttribute("fieldList", server.getFieldList());
        request.setAttribute("sortFieds", server.getSortFields());
    }

    @Override
    protected void add(HttpRequest request) throws Exception
    {
        LuceneServer server = Global.getService(LuceneServer.class);
        request.setAttribute("fieldList", server.getFieldList());
    }

    @Override
    protected void modify(HttpRequest request) throws Exception
    {
        String id = request.getParameter("id");
        
        LuceneServer server = Global.getService(LuceneServer.class);
        request.setAttribute("item", server.item(id));
        request.setAttribute("fieldList", server.getFieldList());
    }

    @Override
    protected void insert(HttpRequest request) throws Exception
    {
        Global.getService(LuceneServer.class).insert(request.getParameterMapOnCNT());
    }

    @Override
    protected void update(HttpRequest request) throws Exception
    {
        Global.getService(LuceneServer.class).update(request.getParameterMapOnCNT());
    }

    @Override
    protected void delete(HttpRequest request) throws Exception
    {
        String id = request.getParameter("id");
        Document doc = Global.getService(LuceneServer.class).item(id);
        if (doc == null)
        {
            request.returnHistory("请选择一个有效的索引");
            return;
        }
        
        Global.getService(LuceneServer.class).delete(id);
    }
}
