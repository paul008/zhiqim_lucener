/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 * 
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.lucener.test.local;

import org.zhiqim.kernel.Global;
import org.zhiqim.kernel.config.Config;
import org.zhiqim.kernel.config.Group;
import org.zhiqim.kernel.config.ItemType;
import org.zhiqim.lucener.LuceneServer;

public class Local
{
    public static void init() throws Exception
    {
        //1.创建配置
        Config config = new Config("lucene", "abc.xml");
        
        Group group = new Group(config, "lucene", "Lucene配置");
        group.add("rootDir", "./datalucene");
        
        Group fGroup = new Group(config, "lucene.field", "Lucene字段配置");
        fGroup.add("labelKeyword", "{type:text,required:false,indexable:true,weight:4.0}", ItemType.PRIVATE, "标签关键词字段配置");
        fGroup.add("customLabelKeyword", "{type:text,required:false,indexable:true,weight:2.0}", ItemType.PRIVATE, "自定义关键词字段配置");
        fGroup.add("longTailKeyword", "{type:text,required:false,indexable:true,weight:8.0}", ItemType.PRIVATE, "长尾关键词字段配置");
        fGroup.add("mediaScore", "{type:int,required:true,indexable:false}", ItemType.PRIVATE, "作品分数字段配置");
        
        Group sGroup = new Group(config, "lucene.sort", "Lucene排序配置");
        sGroup.add("null", "{type:score}", ItemType.PRIVATE, "按搜索得分排序");
        sGroup.add("mediaScore", "{type:int,reverse:true}", ItemType.PRIVATE, "按作品分数排序");
        
        config.addGroup(group);
        config.addGroup(fGroup);
        config.addGroup(sGroup);
        Global.addConfig(config);
        
        //2.创建服务
        LuceneServer server = new LuceneServer();
        server.setId("lucene");
        server.create();
        Global.addService(server);
    }
}
