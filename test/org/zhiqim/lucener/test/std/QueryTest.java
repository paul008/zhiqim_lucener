/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 * 
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.lucener.test.std;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.zhiqim.kernel.Global;
import org.zhiqim.kernel.paging.PageBuilder;
import org.zhiqim.kernel.paging.PageResult;
import org.zhiqim.lucener.LuceneServer;
import org.zhiqim.lucener.test.local.Local;

public class QueryTest
{
    public static void main(String[] args) throws Exception
    {
        Local.init();
        
        LuceneServer server = Global.getService(LuceneServer.class);
        PageResult<Document> result = page(server, 1, 20, "名片");
        System.out.println(result.total());
        
        for (Document doc : result.list())
        {
            System.out.println(doc.get("id"));
        }
    }
    
    /**
     * 查询表对象分页信息
     * 
     * @param pageNo        页码
     * @param pageSize      页数
     * @param q             查询条件
     * @return              分页信息,包括总页数,页码,页数和查询的记录
     * @throws IOException  IO异常
     * @throws ParseException 
     */
    public static PageResult<Document> page(LuceneServer server, int pageNo, int pageSize, String q) throws IOException, ParseException
    {
//        if (Validates.isEmptyBlank(q))
//            return server.page(pageNo, pageSize, null);
        
        try (IndexReader reader = server.getReader())
        {
            if (reader == null)
                return PageBuilder.newResult(pageNo, pageSize);
                
            //1.最简单的查询方法
//            Query query = new TermQuery(new Term("title", q));
            
            //2.带分词的单个字段的查询方法
            //SmartChineseAnalyzer analyzer = new SmartChineseAnalyzer();
            //QueryParser parser = new QueryParser("title", analyzer);
            //Query query = parser.parse(q);
            
            //3.带分词的多个字段的查询方法
            SmartChineseAnalyzer analyzer = new SmartChineseAnalyzer();
            Query query1 = new QueryParser("labelKeyword", analyzer).parse(q);
            Query query2 = new QueryParser("customLabelKeyword", analyzer).parse(q);
            
            BooleanQuery.Builder builder = new BooleanQuery.Builder();
            builder.add(query1, Occur.SHOULD);
            builder.add(query2, Occur.SHOULD);
            Query query = builder.build();
            
            IndexSearcher searcher = new IndexSearcher(reader);
            int total = searcher.count(query);
            if (total < (pageNo-1)*pageSize+1)
            {//小于页数
                return PageBuilder.newResult(total, pageNo, pageSize, new ArrayList<Document>());
            }
            
            TopDocs docs = searcher.search(query, pageNo*pageSize);
            
            List<Document> list = new ArrayList<>();
            for (int i=(pageNo-1)*pageSize;i<pageNo*pageSize && i<total;i++)
            {
                Document document = searcher.doc(docs.scoreDocs[i].doc);
                list.add(document);
            }
            
            return PageBuilder.newResult(total, pageNo, pageSize, list);
        }
    }
}
