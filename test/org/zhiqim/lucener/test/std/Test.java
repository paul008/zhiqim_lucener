/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 * 
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
package org.zhiqim.lucener.test.std;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class Test
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        String str = "名片 金融保险 贷款服务 黄色 白色 通用 色块 横版 个人实用,圆圆 菠萝,CTCF,中腾信,快餐外卖,百联山秀 风景 算白发";


//        Analyzer analyzer1 = new StandardAnalyzer();
//        Analyzer analyzer2 = new StopAnalyzer();
//        Analyzer analyzer3 = new SimpleAnalyzer();
//        Analyzer analyzer4 = new WhitespaceAnalyzer();

        Analyzer analyzer5 = new IKAnalyzer(true);
        
//        getIKAnalyzer(str,analyzer1);
//        getIKAnalyzer(str,analyzer2);
//        getIKAnalyzer(str,analyzer3);
//        getIKAnalyzer(str,analyzer4);
        getIKAnalyzer(str,analyzer5);
    }

    public static void getIKAnalyzer(String str,Analyzer analyzer){
        try{
            //将一个字符串创建成Token流
            TokenStream stream = analyzer.tokenStream("field", new StringReader(str));
            //保存相应词汇
            CharTermAttribute cta = stream.addAttribute(CharTermAttribute.class);
            stream.reset(); 
            while(stream.incrementToken()){
                System.out.print(cta+"|");
            }
            System.out.println();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
