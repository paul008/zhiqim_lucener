### 什么是“Zhiqim Lucener”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zhiqim Lucener是基于Apache Lucene+IKAnalyzer开发的索引服务，采用Zhiqim Framework封装成一个可独立运行和配置的工程，并增加了索引数据的管理，远程通过RMI方式对索引的增删改和搜索，因此大大增加了Lucene的实用性。

<br>

### Zhiqim Lucener有什么优点？
---------------------------------------
1、把Apache Lucene+IKAnalyzer封装成一个独立部署的工程，业务工程通过RMI方式调用，支持增删改索引和查询服务。<br>
2、基于zhiqim framework，有zhiqim_console，可以通过控制台登录对索引和词典进行配置管理。<br>
3、安装简单，特别是有经验的技术人员，配置好boot.home和本地监听端口和目标IP和端口，一键启动（zhiqim.exe/zhiqim.lix）。<br>
4、Windows/Linux都支持，并且经过大量压力测试，可靠性有保障。<br>
<br>

### Zhiqim Lucener安装指南
---------------------------------------
1、要求JDK1.7+,其详细安装教程请前往[【JDK安装教程】](https://zhiqim.org/document/bestcase/jdk.htm)。<br>
2、进往下载发行版：下载请点击[【ZhiqimLucener发行版】](https://zhiqim.org/project/zhiqim_products/zhiqim_lucener/release.htm)。<br>
<br>

### 知启蒙技术框架与交流
---------------------------------------
![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多Zhiqim Tunneler，[【请戳这里】](https://zhiqim.org/project/zhiqim_products/zhiqim_tunneler/tutorial/index.htm)
